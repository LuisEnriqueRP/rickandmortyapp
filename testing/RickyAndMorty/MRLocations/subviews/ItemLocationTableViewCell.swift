//
//  ItemLocationTableViewCell.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

final class ItemLocationTableViewCell: UITableViewCell, ReusableView {
    // MARK: - Private properties
    private lazy var nameLabel: UILabel = makeLabel(forTextStyle: .headline)
    private lazy var dimensionLabel: UILabel = makeLabel(
        forTextStyle: .subheadline)
    private lazy var typeLabel: UILabel = makeLabel(forTextStyle: .footnote)
    
    private lazy var containerStack: UIStackView = {
        let stack = UIStackView().forAutoLayout()
        stack.axis = .vertical
        stack.spacing = 5
        return stack
    }()
    
    // MARK: - Life Cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUserInterface()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    private func configUserInterface() {
        selectionStyle = .none
        self.buildViewHerarchy()
        self.setupConstraints()
    }
    
    public func configData(viewModel: LocationModelData) {
        nameLabel.text = viewModel.name
        dimensionLabel.text = viewModel.dimension
        typeLabel.text = viewModel.type
    }
    
    private func makeLabel(forTextStyle: UIFont.TextStyle) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: forTextStyle)
        label.textColor = UIColor(resource: .rickBlue)
        return label
    }

}

private extension ItemLocationTableViewCell {
    func buildViewHerarchy() {
        containerStack.addArrangedSubview(nameLabel)
        containerStack.addArrangedSubview(dimensionLabel)
        containerStack.addArrangedSubview(typeLabel)
        addSubview(containerStack)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            containerStack.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            containerStack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20),
            containerStack.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10),
            containerStack.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10)
        ])
    }
}

