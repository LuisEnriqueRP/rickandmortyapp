//
//  LocationModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct LocationModel {
    let name: String
    let type: String?
    let dimension: String?
    let url: String
}
