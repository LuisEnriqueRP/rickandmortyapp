//
//  RMLocationsViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

final class RMLocationsViewModel: RMLocationsViewModelProtocol {
    
    private let apiClientService: ApiClientService
    private var urlList: String
    private var lastPageValidation: LastPageValidation = LastPageValidation()
    
    private var locations: [LocationModel] = []
    
    var itemsLocationCount: Int {
        locations.count
    }
    
    var lastPage: Bool {
        lastPageValidation.lastPage
    }
    
    init(state: PassthroughSubject<State, Never>, apiClientService: ApiClientService = ApiClient(), urlList: String) {
        self.urlList = urlList
        self.state = state
        self.apiClientService = apiClientService
    }
    
    var state: PassthroughSubject<State, Never>
    
    func loadViewData() {
        state.send(.loading)
        Task {
            let result = await self.fetch()
            switch result {
            case .success(let locationsResult):
                lastPageValidation.updateLastPage(itemsCount: locationsResult.count)
                locations.append(contentsOf: locationsResult)
                state.send(.success)
            case .failure(let error):
                state.send(.failure(error: error.localizedDescription))
            }
        }
    }
    
}

extension RMLocationsViewModel {
    
    func fetch() async -> Result<[LocationModel], Error> {
        guard !urlList.isEmpty else { return .success([])}
        do {
            let result = try await self.apiClientService.request(url: URL(string: urlList), type: LocationModelMapper.self).toMappedLocationModel()
            urlList = result.info.next ?? .empty
            return .success(result.locations)
        }catch {
            return .failure(error)
        }
        
    }
    
    private func updateStateUI(resetValues: Bool ,response: Result<[LocationModel], Error>) {
        switch response {
        case .success(let locationArray):
            if resetValues { locations.removeAll() }
            lastPageValidation.updateLastPage(itemsCount: locationArray.count)
            locations.append(contentsOf: locationArray)
            state.send(.success)
        case .failure(let error):
            state.send(.failure(error: error.localizedDescription))
        }
    }
    
    // MARK: - Helpers
    func getItemLocationViewModel(row: Int) -> LocationModelData {
        checkAndLoadMoreItems(row: row)
        let location = locations[row]
        let itemLocationViewModel = LocationModelData(location: location)
        return itemLocationViewModel
    }
    
    private func checkAndLoadMoreItems(row: Int) {
        lastPageValidation.checkAndLoadMoreItems(
            row: row, actualItems: locations.count,
            action: loadViewData)
    }
}

