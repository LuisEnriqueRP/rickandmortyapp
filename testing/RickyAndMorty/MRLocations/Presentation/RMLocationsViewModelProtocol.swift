//
//  RMLocationsViewModelProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol RMLocationsViewModelProtocol: BaseViewModel {
    var itemsLocationCount: Int { get }
    var lastPage: Bool { get }
    func getItemLocationViewModel(row: Int) -> LocationModelData
}
