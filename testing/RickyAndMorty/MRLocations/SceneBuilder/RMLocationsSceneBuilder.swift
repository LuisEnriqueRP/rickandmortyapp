//
//  RMLocationsSceneBuilder.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit
import Combine

final class RMLocationsSceneBuilder {
    func build(coordinator: RMLocationsCoordinator, urlString: String) -> RMLocationsController {
        let state = PassthroughSubject<State, Never>()
        let viewModel = RMLocationsViewModel(state: state, urlList: urlString)
        let vc = RMLocationsController(viewModel: viewModel, coordinator: coordinator)
        return vc
    }
}

