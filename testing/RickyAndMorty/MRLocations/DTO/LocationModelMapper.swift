//
//  LocationModelMapper.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct LocationModelMapper: Decodable {
    let info: InfoData
    let results: [LocationModelInfo]
}

extension LocationModelMapper {
    func toMappedLocationModel() -> (info: InfoData, locations: [LocationModel]) {
        let info = InfoData(next: info.next)
        let locations = results.map {
            LocationModel(
                name: $0.name,
                type: $0.type,
                dimension: $0.dimension,
                url: $0.url)
        }
        
        return (info: info, locations: locations)
    }
}
