//
//  LocationModelData.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

struct LocationModelData {
    private(set) var location: LocationModel
    
    var name: String {
        location.name
    }
    
    //TODO: - Remove strings and use Localized
    var dimension: String {
        let safeDimension = location.dimension ?? Localized.unknown
        return "Dimension: \(safeDimension)"
    }
    
    var type: String {
        let safeType = location.type ?? Localized.unknown
        return "Type: \(safeType)"
    }
}
