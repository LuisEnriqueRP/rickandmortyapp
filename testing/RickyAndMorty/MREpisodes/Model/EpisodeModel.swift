//
//  EpisodeModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct EpisodeModel {
    let id: Int
    let name: String
    let airDate: String
    let episode: String
}
