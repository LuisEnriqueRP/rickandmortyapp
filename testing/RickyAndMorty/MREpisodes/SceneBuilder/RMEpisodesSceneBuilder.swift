//
//  RMEpisodesSceneBuilder.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit
import Combine

final class RMEpisodesSceneBuilder {
    func build(coordinator: RMEpisodesCoordinator, urlString: String) -> RMEpisodesController {
        let state = PassthroughSubject<State, Never>()
        let viewModel = RMEpisodesViewModel(state: state, urlList: urlString)
        let vc = RMEpisodesController(viewModel: viewModel, coordinator: coordinator)
        return vc
    }
}
