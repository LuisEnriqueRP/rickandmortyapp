//
//  EpisodeModelMapper.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct EpisodeModelMapper: Decodable {
    let info: InfoData
    let results: [EpisodeModelInfo]
}


extension EpisodeModelMapper {
    func toMappedEpisodeModel() -> (info: InfoData, episodes: [EpisodeModel]) {
        let info = InfoData(next: info.next)
        let episodes = results.map {
            EpisodeModel(
                id: $0.id,
                name: $0.name,
                airDate: $0.airDate,
                episode: $0.episode)
        }
        
        return (info: info, episodes: episodes)
    }
}
