//
//  EpisodeModelData.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct EpisodeModelData {
    private(set) var episode: EpisodeModel
    
    var numberEpisode: String {
        "# \(episode.id)"
    }
    
    var name: String {
        episode.name
    }
    
    var airDate: String {
        episode.airDate
    }
    
    var seasonAndEpisode: String {
        episode.episode
    }
}
