//
//  RMEpisodesViewModelProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol RMEpisodesViewModelProtocol: BaseViewModel {
    var itemsEpisodesCount: Int { get }
    var lastPage: Bool { get }
    func getItemEpisodeViewModel(row: Int) -> EpisodeModelData
}
