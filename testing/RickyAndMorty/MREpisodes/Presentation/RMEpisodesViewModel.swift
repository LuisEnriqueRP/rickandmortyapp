//
//  RMEpisodesViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

class RMEpisodesViewModel: RMEpisodesViewModelProtocol {
    
    var state: PassthroughSubject<State, Never>
    private let apiClientService: ApiClientService
    private var urlList: String
    private var lastPageValidation: LastPageValidation = LastPageValidation()
    
    private var episodes: [EpisodeModel] = []
    var itemsEpisodesCount: Int {
        episodes.count
    }
    
    var lastPage: Bool {
        lastPageValidation.lastPage
    }
    
    init(state: PassthroughSubject<State, Never>, apiClientService: ApiClientService = ApiClient(), urlList: String) {
        self.urlList = urlList
        self.state = state
        self.apiClientService = apiClientService
    }
    
    func loadViewData() {
        state.send(.loading)
        Task {
            let result = await self.fetch()
            switch result {
            case .success(let episodesResult):
                lastPageValidation.updateLastPage(itemsCount: episodesResult.count)
                episodes.append(contentsOf: episodesResult)
                state.send(.success)
            case .failure(let error):
                state.send(.failure(error: error.localizedDescription))
            }
        }
    }
    
    
}

extension RMEpisodesViewModel {
    
    func fetch() async -> Result<[EpisodeModel], Error> {
        guard !urlList.isEmpty else { return .success([])}
        do {
            let result = try await self.apiClientService.request(url: URL(string: urlList), type: EpisodeModelMapper.self).toMappedEpisodeModel()
            urlList = result.info.next ?? .empty
            return .success(result.episodes)
        }catch {
            return .failure(error)
        }
    }
    
    private func updateStateUI(resetValues: Bool ,response: Result<[EpisodeModel], Error>) {
        switch response {
        case .success(let episodesArray):
            if resetValues { episodes.removeAll() }
            lastPageValidation.updateLastPage(itemsCount: episodesArray.count)
            episodes.append(contentsOf: episodesArray)
            state.send(.success)
        case .failure(let error):
            state.send(.failure(error: error.localizedDescription))
        }
    }
    
    // MARK: - Helpers
    func getItemEpisodeViewModel(row: Int) -> EpisodeModelData {
        lastPageValidation.checkAndLoadMoreItems(
            row: row, actualItems: episodes.count,
            action: loadViewData)
        
        let episode = episodes[row]
        return EpisodeModelData(episode: episode)
    }
}
