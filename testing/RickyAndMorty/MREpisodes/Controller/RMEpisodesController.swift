//
//  RMEpisodesController.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit
import Combine

class RMEpisodesController: UITableViewController, MessageAlertDisplayable {
    
    private var cancellable = Set<AnyCancellable>()
    private weak var coordinator: RMEpisodesCoordinator?
    private var viewModel: RMEpisodesViewModelProtocol
    
    init(viewModel: RMEpisodesViewModelProtocol, coordinator: RMEpisodesCoordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(style: .insetGrouped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configUserInterface()
        fetchingState()
        addSpinnerLastCell()
        viewModel.loadViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Episodes"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
    }

}

extension RMEpisodesController {
    private func configUserInterface() {
        view.backgroundColor = UIColor(resource: .nav)
        tableView.register(
            ItemEpisodeTableViewCell.self,
            forCellReuseIdentifier: ItemEpisodeTableViewCell.reuseIdentifier)
    }
    
    private func fetchingState() {
        viewModel
            .state
            .receive(on: RunLoop.main)
            .sink { [weak self] state in
                switch state {
                case .success:
                    self?.tableView.reloadData()
                case .loading: break
                case .failure(error: let error):
                    debugPrint("\(error)")
                    self?.presentAlert(message: error, title: Localized.error)
                }
            }.store(in: &cancellable)
    }
}

// MARK: - Datasource
extension RMEpisodesController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: ItemEpisodeTableViewCell.reuseIdentifier,
                for: indexPath) as? ItemEpisodeTableViewCell
        else { return UITableViewCell() }
        
        let viewModel = viewModel.getItemEpisodeViewModel(row: indexPath.row)
        cell.configData(viewModel: viewModel)
        return cell
    }
    
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        viewModel.itemsEpisodesCount
    }
    
    override func tableView(
        _ tableView: UITableView,
        willDisplay cell: UITableViewCell,
        forRowAt indexPath: IndexPath
    ) {
        tableView.tableFooterView?.isHidden = viewModel.lastPage
    }
}
