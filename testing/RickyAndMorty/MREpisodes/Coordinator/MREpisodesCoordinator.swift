//
//  MREpisodesCoordinator.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

final class RMEpisodesCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var navigationController: CustomNavigationController
    let urlString: String
    
    
    init(navigation: CustomNavigationController, url: String){
        self.navigationController = navigation
        self.urlString = url
    }
    
    func start() {
        let vc = RMEpisodesSceneBuilder().build(coordinator: self, urlString: self.urlString)
        navigationController.pushViewController(vc, animated: true)
    }
    
}
