//
//  ItemEpisodeTableViewCell.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

final class ItemEpisodeTableViewCell: UITableViewCell, ReusableView {
    // MARK: - Private properties
    private lazy var nameLabel: UILabel = {
        let label = UILabel().forAutoLayout()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var containerTagsStackView: UIStackView = {
        let stack = UIStackView().forAutoLayout()
        stack.spacing = 4
        return stack
    }()
    
    private lazy var mainContainerStackView: UIStackView = {
        let stack = UIStackView().forAutoLayout()
        stack.axis = .vertical
        stack.spacing = 5
        return stack
    }()
    
    private lazy var numberEpisodeLabel: UILabel = makeCustomPaddingLabel()
    private lazy var airDateLabel: UILabel = makeCustomPaddingLabel()
    private lazy var seasonAndEpisodeLabel: UILabel = makeCustomPaddingLabel()
    
    // MARK: - Life Cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUserInterface()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    // TODO: - Remove magic numbers
    private func configUserInterface() {
        selectionStyle = .none
        self.buildViewHerarchy()
        self.setupConstraints()
    }
    
    public func configData(viewModel: EpisodeModelData) {
        nameLabel.text = viewModel.name
        numberEpisodeLabel.text = viewModel.numberEpisode
        airDateLabel.text = viewModel.airDate
        seasonAndEpisodeLabel.text = viewModel.seasonAndEpisode
    }
    
    // TODO: - Remove magic numbers
    private func makeCustomPaddingLabel() -> UILabel {
        let label = CustomPaddingLabel()
        label.backgroundColor = .systemBlue
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.edgeInset = UIEdgeInsets(top: 2, left: 10, bottom: 2, right: 10)
        label.layer.cornerRadius = 5
        label.layer.masksToBounds = true
        label.textColor = .white
       return label
    }
}

private extension ItemEpisodeTableViewCell {
    func buildViewHerarchy() {
        containerTagsStackView.addArrangedSubview(numberEpisodeLabel)
        containerTagsStackView.addArrangedSubview(airDateLabel)
        containerTagsStackView.addArrangedSubview(seasonAndEpisodeLabel)
        containerTagsStackView.addArrangedSubview(UIView())
        mainContainerStackView.addArrangedSubview(nameLabel)
        mainContainerStackView.addArrangedSubview(containerTagsStackView)
        
        addSubview(mainContainerStackView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            mainContainerStackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            mainContainerStackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20),
            mainContainerStackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10),
            mainContainerStackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10)
        ])
    }
}
