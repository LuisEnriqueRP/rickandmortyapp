//
//  RMMainSceneBuilder.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit
import Combine

class RMMainSceneBuilder {
    func build(coordinator: RMMainCoordinatorProtocol) -> RMMainViewController {
        let state = PassthroughSubject<State, Never>()
        let viewModel = MainMenuViewModel(state: state)
        let vc = RMMainViewController(viewModel: viewModel, coordinator: coordinator)
        return vc
    }
}
