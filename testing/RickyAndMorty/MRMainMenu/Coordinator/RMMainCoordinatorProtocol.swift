//
//  RMMainCoordinatorProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

enum MenuPaths: String {
    case characters = "characters"
    case episodes = "episodes"
    case locations = "locations"
}

protocol RMMainCoordinatorProtocol: AnyObject {
    func didSelectMenuCell(model: MenuItem)
}
