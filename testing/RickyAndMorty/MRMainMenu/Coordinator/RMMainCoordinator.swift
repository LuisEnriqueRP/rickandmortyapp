//
//  RMMainCoordinator.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit

final class RMMainCoordinator: Coordinator, RMMainCoordinatorProtocol {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: CustomNavigationController
    private var characterCoordinator: RMCharacterCoordinator?
    private var episodesCoordinator: RMEpisodesCoordinator?
    private var locationsCoordinator: RMLocationsCoordinator?
    
    
    init(navigation: CustomNavigationController){
        self.navigationController = navigation
    }
    
    func start() {
        let vc = RMMainSceneBuilder().build(coordinator: self)
        navigationController.pushViewController(vc, animated: false)
    }
    
    func didSelectMenuCell(model: MenuItem) {
        switch MenuPaths(rawValue: model.title) {
        case .characters:
            coordinateToCharacters(url: model.url)
        case .episodes:
            coordinateToEpisodes(url: model.url)
        case .locations:
            coordinateToLocations(url: model.url)
        default:
            break
        }
    }
    
    func coordinateToCharacters(url: String) {
         characterCoordinator = RMCharacterCoordinator(navigation: navigationController, url: url)
        characterCoordinator?.start()
    }
    
    func coordinateToEpisodes(url: String) {
         episodesCoordinator = RMEpisodesCoordinator(navigation: navigationController, url: url)
        episodesCoordinator?.start()
    }
    
    func coordinateToLocations(url: String) {
         locationsCoordinator = RMLocationsCoordinator(navigation: navigationController, url: url)
        locationsCoordinator?.start()
    }
    
}
