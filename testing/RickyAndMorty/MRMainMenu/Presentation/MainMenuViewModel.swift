//
//  MainMenuViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 24/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

class MainMenuViewModel: MainMenuViewModelProtocol {
    var menuItems: [MenuItem] = []
    private let apiClientService: ApiClientService
    
    var state: PassthroughSubject<State, Never>
    
    var menuItemsCount: Int {
        menuItems.count
    }
    
    init(state: PassthroughSubject<State, Never>, apiClientService: ApiClientService = ApiClient()) {
        self.state = state
        self.apiClientService = apiClientService
    }
    
    func loadViewData() {
        Task {
            let result = await self.fetch()
            updateFetchedData(result: result)
        }
    }
    
    func getItemMenuViewMode(indexPath: IndexPath) -> MenuItemData {
        let menuItem = menuItems[indexPath.row]
        return MenuItemData(menuItem: menuItem)
    }
    
    func getMenuItem(indexPath: IndexPath) -> MenuItem {
        menuItems[indexPath.row]
    }
    
    
}

//MARK: Methods
private extension MainMenuViewModel {
    
    func fetch() async -> Result<[MenuItem], Error>{
        do {
            let menuResponse = try await apiClientService.request(url: URL(string: Endpoint.baseUrl), type: MenuItemMapper.self).toMappedMenuItem()
            return .success(menuResponse)
        } catch {
            return .failure(error)
        }
    }
    
    func updateFetchedData(result: Result<[MenuItem], Error>) {
        switch result {
        case .success(let menuItems):
            self.menuItems = menuItems
            state.send(.success)
        case .failure(let error):
            state.send(.failure(error: error.localizedDescription))
        }
    }
}
