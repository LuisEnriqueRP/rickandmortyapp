//
//  MainMenuViewModelProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 24/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

protocol MainMenuViewModelProtocol: BaseViewModel {
    var menuItemsCount: Int { get }
    func getItemMenuViewMode(indexPath: IndexPath) -> MenuItemData
    func getMenuItem(indexPath: IndexPath) -> MenuItem
}
