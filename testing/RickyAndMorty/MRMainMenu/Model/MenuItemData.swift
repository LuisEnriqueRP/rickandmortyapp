//
//  MenuItemData.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 24/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct MenuItemData {
    private let menuItem: MenuItem
    
    init(menuItem: MenuItem) {
        self.menuItem = menuItem
    }
    
    var title: String {
        menuItem.title.capitalized
    }
    
    var imageName:String {
        menuItem.title
    }
}
