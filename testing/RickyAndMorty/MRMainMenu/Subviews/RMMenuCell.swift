//
//  RMCharacterCell.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 23/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

class RMMenuCell: UICollectionViewCell, ReusableView {
    
    lazy var characterImage: UIImageView = {
        let image = UIImageView().forAutoLayout()
        return image
    }()
    
    lazy var titleLabel: UILabel = {
        let title = UILabel().forAutoLayout()
        title.textColor = .white
        title.sizeToFit()
        title.font = UIFont(name: "Kohinoor Bangla Semibold", size: 20.0)
        return title
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        setupView()
    }
    private func setupView() {
        buildViewHerarchy()
        setupConstraints()
    }

    func setupImage(mediaImageURLStr: String) {
        self.characterImage.getImage(from: mediaImageURLStr)
    }
    
    func setupMenuCell(menuItem: MenuItemData) {
        self.titleLabel.text = menuItem.title
    }

}

extension RMMenuCell { // Setup views
    private func buildViewHerarchy() {
        self.addBlur(style: .light)
        self.setRoundedCorners(with: 8.0)
        self.contentView.addSubview(titleLabel)
        self.clipsToBounds = true
        
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            self.titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
}

