//
//  RMMainViewController.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 23/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit
import Combine

final class RMMainViewController: UIViewController, MessageAlertDisplayable {
    
    private var cancellable = Set<AnyCancellable>()
    private weak var coordinator: RMMainCoordinatorProtocol?
    private var viewModel: MainMenuViewModelProtocol
    
    init(viewModel: MainMenuViewModelProtocol, coordinator: RMMainCoordinatorProtocol) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var itemsLayout: UICollectionViewCompositionalLayout {
         RMCompositionalLayouts().listCollectionLayout()
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let controller = UIRefreshControl()
        controller.addTarget(self, action: #selector(self.fetchMainData), for: .valueChanged)
        controller.tintColor = .white
        return controller
    }()
    
    
    private lazy var rmCollectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: itemsLayout).forAutoLayout()
        collection.register(RMMenuCell.self)
        collection.refreshControl = refreshControl
        collection.backgroundColor = .clear
        collection.delegate = self
        collection.dataSource = self
        collection.showsVerticalScrollIndicator = false
        return collection
        }()
    
    private lazy var background: UIImageView = {
        let image = UIImageView(image: UIImage(resource: .mainMenuBackground)).forAutoLayout()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.fetchingState()
        self.fetchMainData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let logo = UIImage(resource: .titleMRView)
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    
    private func fetchingState() {
        viewModel
            .state
            .receive(on: RunLoop.main)
            .sink { [weak self] state in
                self?.refreshControl.endRefreshing()
                switch state {
                case .success:
                    self?.rmCollectionView.reloadData()
                case .loading:
                    self?.refreshControl.beginRefreshing()
                case .failure(error: let error): 
                    debugPrint("\(error)")
                    self?.presentAlert(message: error, title: Localized.error)
                }
            }.store(in: &cancellable)
    }
}

//MARK: Methods
private extension RMMainViewController {
    @objc func fetchMainData() {
        viewModel.loadViewData()
    }
}


extension RMMainViewController {
    
    func setupUI() {
        self.view.backgroundColor = .systemBackground
        buildViewHerarchy()
        setupContraints()
    }
    
    func buildViewHerarchy() {
        self.view.addSubview(background)
        self.view.addSubview(rmCollectionView)
    }
    
    func setupContraints() {
        background.fillSuperview()
        NSLayoutConstraint.activate([
            rmCollectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            rmCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            rmCollectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12),
            rmCollectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12)
        ])
    }
}

extension RMMainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.menuItemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RMMenuCell.reuseIdentifier, for: indexPath) as? RMMenuCell else { return UICollectionViewCell() }
        
        let menuItemModel = viewModel.getItemMenuViewMode(indexPath: indexPath)
        cell.setupMenuCell(menuItem: menuItemModel)
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        let model = viewModel.getMenuItem(indexPath: indexPath)
        coordinator?.didSelectMenuCell(model: model)
    }
}

