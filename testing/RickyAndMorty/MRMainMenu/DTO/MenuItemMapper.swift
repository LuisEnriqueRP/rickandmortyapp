//
//  MenuItemMapper.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct MenuItemMapper: Decodable {
    let characters: String
    let locations: String
    let episodes: String
}

extension MenuItemMapper: PropertyIterator {
    func toMappedMenuItem() -> [MenuItem] {
        return self.dictionaryProperties().map { dictionary in
            let title = dictionary.key
            let url: String = (dictionary.value as? String) ?? String()
            return MenuItem(title: title, url: url)
        }
    }
}
