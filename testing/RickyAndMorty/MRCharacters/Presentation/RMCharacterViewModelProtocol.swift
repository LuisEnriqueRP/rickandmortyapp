//
//  RMCharacterViewModelProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol RMCharacterViewModelProtocol: BaseViewModel {
    var itemCharactersCount: Int { get }
    var lastPage: Bool { get }
    var isFetching: Bool { get set }
    func firstFetch()
    func getItemMenuViewModel(row: Int) -> CharacterModelData
    func getUrlDetail(row: Int) -> String
}
