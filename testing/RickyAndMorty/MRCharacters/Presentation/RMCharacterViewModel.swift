//
//  RMCharacterViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

class RMCharacterViewModel: RMCharacterViewModelProtocol {
    
    var state: PassthroughSubject<State, Never>
    var isFetching: Bool = false
    private let apiClientService: ApiClientService
    private var urlList: String
    private var lastPageValidation: LastPageValidation = LastPageValidation()
    private var characters: [CharacterModel] = []
    
    var lastPage: Bool {
        lastPageValidation.lastPage
    }
    
    var itemCharactersCount: Int {
        characters.count
    }
    
    init(state: PassthroughSubject<State, Never>, apiClientService: ApiClientService = ApiClient(), urlList: String) {
        self.urlList = urlList
        self.state = state
        self.apiClientService = apiClientService
    }
    
    func getItemMenuViewModel(row: Int) -> CharacterModelData {
        checkAndLoadMoreCharacters(row: row)
        return makeItemCharacterData(row: row)
    }
    
    func getUrlDetail(row: Int) -> String {
        let character = characters[row]
        return character.urlCharacter
    }
    
    func firstFetch() {
        self.fetchData(reset: true)
    }
    
    func loadViewData() {
        self.fetchData()
    }
    
    func fetchData(reset: Bool = false) {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self else { return }
            self.state.send(.loading)
            Task {
                let result = await self.fetch()
                await MainActor.run { [weak self] in
                    self?.updateStateUI(resetValues: reset, response: result)
                }
            }
        }
    }
    
    private func checkAndLoadMoreCharacters(row: Int) {
        if !isFetching {
            lastPageValidation.checkAndLoadMoreItems(
                row: row,
                actualItems: characters.count,
                action: loadViewData)
        }
    }
    
}

private extension RMCharacterViewModel {
  
    func fetch() async -> Result<[CharacterModel], Error> {
        guard !urlList.isEmpty else { return .success([]) }
        do {
            let charactersResponse =  try await apiClientService.request(url: URL(string: urlList), type: CharacterModelMapper.self).toMappedCharacterModel()
            urlList = charactersResponse.info.next ?? .empty
            return .success(charactersResponse.character)
        } catch {
            return .failure(error)
        }
        
    }
    
    private func makeItemCharacterData(row: Int) -> CharacterModelData {
        let character = characters[row]
        return CharacterModelData(character: character)
    }
}

extension RMCharacterViewModel {
    
    private func updateStateUI(resetValues: Bool ,response: Result<[CharacterModel], Error>) {
        switch response {
        case .success(let charactersArray):
            if resetValues { characters.removeAll() }
            lastPageValidation.updateLastPage(itemsCount: charactersArray.count)
            characters.append(contentsOf: charactersArray)
            state.send(.success)
        case .failure(let error):
            state.send(.failure(error: error.localizedDescription))
        }
    }
}
