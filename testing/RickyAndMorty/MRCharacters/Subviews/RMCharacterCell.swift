//
//  RMCharacterCell.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

class RMCharacterCell: UICollectionViewCell, ReusableView {
    private lazy var characterImageView: UIImageView = {
        let image = UIImageView().forAutoLayout()
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        return image
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel().forAutoLayout()
        label.font = .preferredFont(forTextStyle: .headline)
        label.textColor = .white
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel().forAutoLayout()
        label.font = .preferredFont(forTextStyle: .callout)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .secondaryLabel
        return label
    }()
    
    private lazy var specieLabel: UILabel = {
        let label = UILabel().forAutoLayout()
        label.font = .preferredFont(forTextStyle: .callout)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .secondaryLabel
        return label
    }()
    
    private lazy var labelsVerticalStackView: UIStackView = {
        let stackView = UIStackView().forAutoLayout()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .leading
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHerarchy()
        setupConstraints()
        configureContents()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("Fatal error")
    }
    
    private func buildViewHerarchy() {
        contentView.addSubview(characterImageView)
        contentView.addSubview(labelsVerticalStackView)
        labelsVerticalStackView.addArrangedSubview(nameLabel)
        labelsVerticalStackView.addArrangedSubview(statusLabel)
        labelsVerticalStackView.addArrangedSubview(specieLabel)
    }
    
    private func setupConstraints() {
        // Compute image ratio
        let ratio = characterImageView.intrinsicContentSize.height / characterImageView.intrinsicContentSize.width
        NSLayoutConstraint.activate([
            characterImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            characterImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            characterImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            characterImageView.heightAnchor.constraint(equalTo: characterImageView.widthAnchor, multiplier: ratio),
            labelsVerticalStackView.topAnchor.constraint(equalTo: characterImageView.bottomAnchor, constant: 8),
            labelsVerticalStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            labelsVerticalStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
        ])
    }
    
    private func configureContents() {
        layer.cornerRadius = 10
        clipsToBounds = true
        contentView.backgroundColor = UIColor(resource: .rickBlue)

        
    }
    
    func configData(viewModel: CharacterModelData) {
        nameLabel.text = viewModel.name
        specieLabel.text = viewModel.specie
        statusLabel.text = viewModel.status
        characterImageView.getImage(from: viewModel.imageUrl)
    }
}
