//
//  RMCharacterController.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit
import Combine

class RMCharacterController: UIViewController, MessageAlertDisplayable {
    
    private var cancellable = Set<AnyCancellable>()
    private weak var coordinator: RMCharacterCoordinatorProtocol?
    private var viewModel: RMCharacterViewModelProtocol
    
    private var itemsLayout: UICollectionViewCompositionalLayout {
        RMCompositionalLayouts().characterCellCompositionalLayout()
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let controller = UIRefreshControl()
        controller.addTarget(self, action: #selector(self.fetchMainData), for: .valueChanged)
        controller.tintColor = .white
        return controller
    }()
    
    private lazy var rmCharacterCollection: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: itemsLayout).forAutoLayout()
        collection.register(RMCharacterCell.self)
        collection.refreshControl = refreshControl
        collection.backgroundColor = .clear
        collection.delegate = self
        collection.dataSource = self
        collection.showsVerticalScrollIndicator = false
        return collection
        }()
    
    init(viewModel: RMCharacterViewModelProtocol, coordinator: RMCharacterCoordinatorProtocol) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViewHerarchy()
        fetchingState()
        viewModel.loadViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Characters"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
        self.view.backgroundColor = UIColor(resource: .nav)
    }
    
    
    private func fetchingState() {
        viewModel
            .state
            .receive(on: RunLoop.main)
            .sink { [weak self] state in
                self?.viewModel.isFetching = false
                self?.refreshControl.endRefreshing()
                switch state {
                case .success:
                    self?.rmCharacterCollection.reloadData()
                case .loading:
                    self?.viewModel.isFetching = true
                    self?.refreshControl.beginRefreshing()
                case .failure(error: let error):
                    debugPrint("\(error)")
                    self?.presentAlert(message: error, title: Localized.error)
                }
            }.store(in: &cancellable)
    }

}

//MARK: Methods
private extension RMCharacterController {
    @objc func fetchMainData() {
        viewModel.firstFetch()
    }
}
//MARK: UI setup
private extension RMCharacterController {
    func buildViewHerarchy() {
        self.view.addSubview(rmCharacterCollection)
        self.setupConstraints()
    }
    
    func setupConstraints() {
        self.rmCharacterCollection.fillSuperview()
    }
}


extension RMCharacterController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.itemCharactersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RMCharacterCell.reuseIdentifier, for: indexPath) as? RMCharacterCell else { return UICollectionViewCell() }
        let row = indexPath.row
        let itemViewModel = viewModel.getItemMenuViewModel(row: row)
        cell.configData(viewModel: itemViewModel)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row = indexPath.row
        let urlDetail = viewModel.getUrlDetail(row: row)
        self.coordinator?.didSelectCell(urlDetail: urlDetail)
    }
}

