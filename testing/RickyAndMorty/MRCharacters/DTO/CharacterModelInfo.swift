//
//  CharacterModelInfo.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct CharacterModelInfo: Decodable {
    let id: Int
    let name: String
    let status: String?
    let species: String?
    let image: String?
    let url: String
    let origin: OriginModelInfo
    let location: LocationModelInfo
}

struct OriginModelInfo: Decodable {
    let name: String
    let url: String
}

