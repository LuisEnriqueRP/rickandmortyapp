//
//  CharacterModelMapper.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct CharacterModelMapper: Decodable {
    let results: [CharacterModelInfo]
    let info: InfoData
}

extension CharacterModelMapper {
    func toMappedCharacterModel() -> (info: InfoData, character: [CharacterModel]) {
        let charactersModel = results.map {
            let statusCharacter = CharacterStatus(status: $0.status)
            let specie = Specie(specie: $0.species)
            let origin = Origin(name: $0.origin.name, url: $0.origin.url)
            let location = Location(
                name: $0.location.name,
                type: $0.location.type,
                dimension: $0.location.dimension,
                url: $0.location.url)
           return CharacterModel(
                id: $0.id,
                name: $0.name,
                status: statusCharacter,
                specie: specie,
                urlCharacter: $0.url,
                urlImage: $0.image,
                origin: origin,
                location: location)
        }
        
        let info = InfoData(next: info.next)
        return (info: info, character: charactersModel)
    }
}

