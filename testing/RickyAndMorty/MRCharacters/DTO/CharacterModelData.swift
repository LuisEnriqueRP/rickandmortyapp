//
//  CharacterModelData.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit

struct CharacterModelData {
    private(set) var character: CharacterModel

    var name: String {
        character.name
    }
    
    var specie: String {
        character.specie.description
    }
    
    var status: String {
        character.status?.description ?? ""
    }
    
    var imageUrl: String {
        character.urlImage ?? ""
    }
}
