//
//  CharacterModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct CharacterModel {
    let id: Int
    let name: String
    let status: CharacterStatus?
    let specie: Specie
    let urlCharacter: String
    let urlImage: String?
    let origin: Origin
    let location: Location
}

enum CharacterStatus: String {
    case alive
    case dead
    case unknown
    
    init?(status: String?) {
        guard
            let status = CharacterStatus(rawValue: status?.lowercased() ?? .empty)
        else {
            return nil
        }
        self = status
    }
}

extension CharacterStatus: CustomStringConvertible {
    var description: String {
        switch self {
        case .alive:
            return Localized.sAlive
        case .dead:
            return Localized.sDead
        case .unknown:
            return Localized.sUnknown
        }
    }
}

enum Specie {
    case alien
    case human
    case other(type: String?)
    
    init(specie: String?){
        switch specie {
        case Localized.alien:
            self = .alien
        case Localized.human:
            self = .human
        default:
            self = .other(type: specie)
        }
    }
}

extension Specie: CustomStringConvertible {
    var description: String {
        switch self {
        case .alien:
            return Localized.specieAlien
        case .human:
            return Localized.specieHuman
        case .other(let typeSpecie):
            let typeSpecie = typeSpecie ?? Localized.unknown
            return "\(Localized.specie) \(typeSpecie)"
        }
    }
}

struct Origin {
    let name: String
    let url: String
}

struct Location {
    let name: String
    let type: String?
    let dimension: String?
    let url: String
}

struct InfoData: Decodable {
    let next: String?
}
