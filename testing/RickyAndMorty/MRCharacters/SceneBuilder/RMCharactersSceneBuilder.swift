//
//  RMCharactersSceneBuilder.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit
import Combine

class RMCharactersSceneBuilder {
    func build(coordinator: RMCharacterCoordinatorProtocol, urlString: String) -> RMCharacterController {
        let state = PassthroughSubject<State, Never>()
        let viewModel = RMCharacterViewModel(state: state, urlList: urlString)
        let vc = RMCharacterController(viewModel: viewModel, coordinator: coordinator)
        return vc
    }
}

