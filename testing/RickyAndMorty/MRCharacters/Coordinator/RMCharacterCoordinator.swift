//
//  RMCharacterCoordinator.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

final class RMCharacterCoordinator: RMCharacterCoordinatorProtocol {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: CustomNavigationController
    let urlString: String
    private var detailCoordinator: RMCharacterDetailCoordinator?
    
    
    init(navigation: CustomNavigationController, url: String){
        self.navigationController = navigation
        self.urlString = url
    }
    
    func start() {
        let vc = RMCharactersSceneBuilder().build(coordinator: self, urlString: self.urlString)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func didSelectCell(urlDetail: String) {
        detailCoordinator = RMCharacterDetailCoordinator(navigation: navigationController, url: urlDetail)
        detailCoordinator?.start()
    }
}
