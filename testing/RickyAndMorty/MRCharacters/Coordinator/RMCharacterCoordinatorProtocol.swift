//
//  RMCharacterCoordinatorProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol RMCharacterCoordinatorProtocol: Coordinator {
    func didSelectCell(urlDetail: String)
}
