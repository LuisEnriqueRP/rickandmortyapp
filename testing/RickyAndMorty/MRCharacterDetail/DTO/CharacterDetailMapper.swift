//
//  CharacterDetailMapper.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

extension CharacterModelInfo {
    func toCharacterDetailModelMapped() -> CharacterModel {
        let statusCharacter = CharacterStatus(status: status)
        let specie = Specie(specie: species)
        let origin = Origin(name: origin.name, url: origin.url)
        let location = Location(
            name: location.name,
            type: location.type,
            dimension: location.dimension,
            url: location.url)
        
        return CharacterModel(
            id: id,
            name: name,
            status: statusCharacter,
            specie: specie,
            urlCharacter: url,
            urlImage: image,
            origin: origin,
            location: location)
    }
}
