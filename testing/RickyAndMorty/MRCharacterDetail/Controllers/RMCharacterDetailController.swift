//
//  RMCharacterDetailController.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit
import Combine

class RMCharacterDetailController: UIViewController, SpinnerDisplayable, MessageAlertDisplayable {
    
    private lazy var InfoStackView: UIStackView = {
        let view = UIStackView().forAutoLayout()
        view.axis = .vertical
        view.isLayoutMarginsRelativeArrangement = true
        view.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)
        view.spacing = 10
        view.distribution = .fill
        view.backgroundColor = UIColor(resource: .rickBlue)
        view.clipsToBounds = true
        view.specificRoundedCorners(at: [.layerMinXMinYCorner, .layerMaxXMinYCorner], with: 13.0)
        return view
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private lazy var specieLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private lazy var originLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        return label
    }()
    
    private lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        return label
    }()
    
    
    // MARK: - Constants
       struct Constants {
           static fileprivate let headerHeight: CGFloat = 250
       }

       private var scrollView: UIScrollView!
       private var headerContainerView: UIView!
       private var headerImageView: UIImageView!
       private var headerTopConstraint: NSLayoutConstraint!
       private var headerHeightConstraint: NSLayoutConstraint!
    
    private var cancellable = Set<AnyCancellable>()
    private weak var coordinator: RMCharacterDetailCoordinator?
    private var viewModel: RMCharacterDetailViewModelProtocol
    
    init(viewModel: RMCharacterDetailViewModelProtocol, coordinator: RMCharacterDetailCoordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func buildViewHerarchy() {
        self.view.backgroundColor = UIColor(resource: .rickBlue)
        scrollView = createScrollView()
        headerContainerView = createHeaderContainerView()
        headerImageView = createHeaderImageView()
        headerContainerView.addSubview(headerImageView)
        scrollView.addSubview(headerContainerView)
        view.addSubview(scrollView)
        scrollView.addSubview(InfoStackView)
        InfoStackView.addArrangedSubview(nameLabel)
        InfoStackView.addArrangedSubview(stateLabel)
        InfoStackView.addArrangedSubview(specieLabel)
        InfoStackView.addArrangedSubview(originLabel)
        InfoStackView.addArrangedSubview(locationLabel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViewHerarchy()
        setupConstraints()
        fetchingState()
        viewModel.loadViewData()
    }
}

private extension RMCharacterDetailController {
    private func fetchingState() {
        viewModel
            .state
            .receive(on: RunLoop.main)
            .sink { [weak self] state in
                self?.hideSpinner()
                switch state {
                case .success:
                    self?.configData()
                case .loading:
                    self?.showSpinner()
                case .failure(error: let error):
                    self?.presentAlert(message: error, title: "Error")
                }
            }.store(in: &cancellable)
    }
    
    private func configData() {
        headerImageView.getImage(from: viewModel.imageUrl)
        nameLabel.text = viewModel.nameCharacter
        stateLabel.text = viewModel.status
        specieLabel.text = viewModel.specie
        originLabel.text = viewModel.origin
        locationLabel.text = viewModel.location
    }
}

 private extension RMCharacterDetailController {
        func createScrollView() -> UIScrollView {
            let scrollView = UIScrollView().forAutoLayout()
            scrollView.backgroundColor = UIColor(resource: .rickBlue)
            scrollView.delegate = self
            scrollView.alwaysBounceVertical = true
            return scrollView
        }

        func createHeaderContainerView() -> UIView {
            let view = UIView().forAutoLayout()
            view.clipsToBounds = true
            view.layer.cornerRadius = 20
            return view
        }

        func createHeaderImageView() -> UIImageView {
            let imageView = UIImageView().forAutoLayout()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
        }

        func setupConstraints() {
            let scrollViewConstraints: [NSLayoutConstraint] = [
                scrollView.topAnchor.constraint(equalTo: view.topAnchor),
                scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
            NSLayoutConstraint.activate(scrollViewConstraints)

            headerTopConstraint = headerContainerView.topAnchor.constraint(equalTo: view.topAnchor)
            headerHeightConstraint = headerContainerView.heightAnchor.constraint(equalToConstant: 210)
            let headerContainerViewConstraints: [NSLayoutConstraint] = [
                headerTopConstraint,
                headerContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
                headerHeightConstraint
            ]
            NSLayoutConstraint.activate(headerContainerViewConstraints)

            let headerImageViewConstraints: [NSLayoutConstraint] = [
                headerImageView.topAnchor.constraint(equalTo: headerContainerView.topAnchor),
                headerImageView.leadingAnchor.constraint(equalTo: headerContainerView.leadingAnchor),
                headerImageView.trailingAnchor.constraint(equalTo: headerContainerView.trailingAnchor),
                headerImageView.bottomAnchor.constraint(equalTo: headerContainerView.bottomAnchor)
            ]
            NSLayoutConstraint.activate(headerImageViewConstraints)

            let labelConstraints: [NSLayoutConstraint] = [
                nameLabel.topAnchor.constraint(equalTo: headerContainerView.bottomAnchor, constant: 20.0),
                nameLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1.0),
            ]
            NSLayoutConstraint.activate(labelConstraints)
        }
    }

    extension RMCharacterDetailController: UIScrollViewDelegate {
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView.contentOffset.y < 0.0 {
                headerHeightConstraint?.constant = Constants.headerHeight - scrollView.contentOffset.y
            } else {
                let parallaxFactor: CGFloat = 0.25
                let offsetY = scrollView.contentOffset.y * parallaxFactor
                let minOffsetY: CGFloat = 8.0
                let availableOffset = min(offsetY, minOffsetY)
                let contentRectOffsetY = availableOffset / Constants.headerHeight
                headerTopConstraint?.constant = view.frame.origin.y
                headerHeightConstraint?.constant = Constants.headerHeight - scrollView.contentOffset.y
                headerImageView.layer.contentsRect = CGRect(x: 0, y: -contentRectOffsetY, width: 1, height: 1)
            }
        }
    }
