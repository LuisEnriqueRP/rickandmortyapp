//
//  RMCharacterDetailSceneBuilder.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

final class RMCharacterDetailSceneBuilder {
    func build(coordinator: RMCharacterDetailCoordinator, urlString: String) -> RMCharacterDetailController {
        let state = PassthroughSubject<State, Never>()
        let viewModel = RMCharacterDetailViewModel(state: state, currentUrl: urlString)
        let vc = RMCharacterDetailController(viewModel: viewModel, coordinator: coordinator)
        return vc
    }
}
