//
//  RMCharacterDetailViewModelProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol RMCharacterDetailViewModelProtocol: BaseViewModel {
    var nameCharacter: String { get }
    var status: String { get }
    var specie: String { get }
    var imageUrl: String { get }
    var origin: String { get }
    var location: String { get }
}
