//
//  RMCharacterDetailViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

final class RMCharacterDetailViewModel: RMCharacterDetailViewModelProtocol {
    
    // MARK: - Internal properties
    var state: PassthroughSubject<State, Never>
    
    var nameCharacter: String {
        character?.name ?? .empty
    }
    
    var status: String {
        character?.status?.description ?? .empty
    }
    
    var specie: String {
        character?.specie.description ?? .empty
    }
    
    var imageUrl: String {
        character?.urlImage ?? .empty
    }
    
    var origin: String {
        character?.origin.name ?? .empty
    }
    
    var location: String {
        character?.location.name ?? .empty
    }
    
    // MARK: - Private properties
    private var character: CharacterModel?
    private var currentUrl: String
    private let apiClientService: ApiClientService
    
    init(state: PassthroughSubject<State, Never>, apiClientService: ApiClientService = ApiClient(), currentUrl: String) {
        self.state = state
        self.apiClientService = apiClientService
        self.currentUrl = currentUrl
    }
    
    func loadViewData() {
        state.send(.loading)
        Task {
            do {
                let characterResult = try await self.fetch()
                character = characterResult
                state.send(.success)
            } catch {
                state.send(.failure(error: error.localizedDescription))
            }
        }
    }
}

extension RMCharacterDetailViewModel {
    
    func fetch() async throws -> CharacterModel {
            let result = try await self.apiClientService.request(
                url: URL(string: currentUrl),
                type: CharacterModelInfo.self).toCharacterDetailModelMapped()
            return result
    }
}

