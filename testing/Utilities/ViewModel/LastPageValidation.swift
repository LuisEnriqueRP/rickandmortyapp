//
//  LastPageValidation.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct LastPageValidation: LastPageValidationProtocol {
    private var threshold = 5
    var lastPage: Bool = false
    
    mutating func updateLastPage(itemsCount: Int) {
        if itemsCount == Int.zero {
            lastPage = true
        }
    }
    
    func checkAndLoadMoreItems(
        row: Int,
        actualItems: Int,
        action: () -> Void
    ) {
        guard !lastPage else { return }
        let limit = actualItems - threshold
        if limit == row {
            action()
        }
    }
}
