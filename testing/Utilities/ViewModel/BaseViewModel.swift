//
//  BaseViewModel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import Combine

protocol BaseViewModel {
    var state: PassthroughSubject<State, Never> { get }
    func loadViewData()
}
