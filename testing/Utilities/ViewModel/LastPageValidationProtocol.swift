//
//  LastPageValidationProtocol.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

protocol LastPageValidationProtocol {
    var lastPage: Bool { get }
    mutating func updateLastPage(itemsCount: Int)
    func checkAndLoadMoreItems(
        row: Int,
        actualItems: Int,
        action: () -> Void)
}
