//
//  State.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 24/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

enum State {
    case success
    case loading
    case failure(error: String)
}
