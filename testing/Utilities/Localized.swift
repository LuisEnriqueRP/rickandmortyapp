//
//  Localized.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 25/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation

struct Localized {
    static let error = "Error"
    static let okButton = "Ok"
    
    static let alien = "Alien"
    static let human = "Human"
    static let unknown = "Unknown"
    
    static let sAlive = "Status: 🟢 Alive"
    static let sDead = "Status: 🪦 Dead"
    static let sUnknown = "Status: ❓ Unknown"
    
    static let specieHuman = "Specie: 👤 Human"
    static let specieAlien = "Specie: 👽 Alien"
    static let specie = "Specie:"
    
    static let locations = "Locations"
}
