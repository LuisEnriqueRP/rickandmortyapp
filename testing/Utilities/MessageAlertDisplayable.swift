//
//  MessageAlertDisplayable.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 23/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import Foundation
import UIKit


protocol MessageAlertDisplayable { }

extension MessageAlertDisplayable where Self: UIViewController {
    func presentAlert(message: String, title: String) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: Localized.okButton, style: .default)
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
}
