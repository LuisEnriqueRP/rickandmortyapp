//
//  CustomPaddingLabel.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 26/02/24.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

final class CustomPaddingLabel: UILabel {
    var edgeInset: UIEdgeInsets = .zero

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(
            top: edgeInset.top,
            left: edgeInset.left,
            bottom: edgeInset.bottom,
            right: edgeInset.right)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(
            width: size.width + edgeInset.left + edgeInset.right,
            height: size.height + edgeInset.top + edgeInset.bottom)
    }
}
