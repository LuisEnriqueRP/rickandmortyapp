//
//  RMCompositionalLayouts.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 06/08/20.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

class RMCompositionalLayouts {
    func listCollectionLayout() -> UICollectionViewCompositionalLayout {
                let inset : CGFloat = 5
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                      heightDimension: .fractionalWidth(2/3))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
                let groupFractionalWidth =  1.0
                let groupFractionalHeight: Float = 2/8
                let groupSize = NSCollectionLayoutSize(
                  widthDimension: .fractionalWidth(CGFloat(groupFractionalWidth)),
                  heightDimension: .fractionalWidth(CGFloat(groupFractionalHeight)))
                let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
                group.contentInsets = NSDirectionalEdgeInsets(top: inset, leading: inset, bottom: inset, trailing: inset)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                heightDimension: .estimated(50))
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .none
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    func characterCellCompositionalLayout() -> UICollectionViewCompositionalLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.33),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.57))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
}
