//
//  MainCoordinator.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 05/08/20.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit
import Combine

final class MainCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
    var childCoordinators = [Coordinator]()
    var navigationController: CustomNavigationController
    private var mainCoordinator: RMMainCoordinator?
    

    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.delegate = self
        mainCoordinator = RMMainCoordinator(navigation: navigationController)
        mainCoordinator?.start()
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in
            childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at:index)
                break
            }
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool){
        guard let fromNavigationController = navigationController.transitionCoordinator?.viewController(forKey: .from) else { return }
        
        if navigationController.viewControllers.contains(fromNavigationController) {
            return
        }
    }
}

