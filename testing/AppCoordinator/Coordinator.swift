//
//  Coordinator.swift
//  RickAndMortyApp
//
//  Created by Kikis Ricaño on 05/08/20.
//  Copyright © 2024 Kikis Ricaño. All rights reserved.
//

import UIKit

protocol Coordinator : AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: CustomNavigationController { get set }

    func start()
}
